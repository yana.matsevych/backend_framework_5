package apache_poi_excel;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class WritingDataToExelFile {
    public static void main(String[] args) throws IOException {


        //getting file path
        String excelPath = "test_data/WriteData.xlsx";

        //created an workbook object
        XSSFWorkbook workbook = new XSSFWorkbook();

        //created sheet on the workbook
        XSSFSheet sheet = workbook.createSheet("Sheet1");

        // go to specific row on that sheet
        XSSFRow row = sheet.createRow(1); // by index

        // go to specific cell on that row
        XSSFCell cell = row.createCell(1); // by index

        //write the value on that cell
        cell.setCellValue("Tech Global");

        //store the file path into the system
        FileOutputStream fileOutputStream = new FileOutputStream(excelPath);

        //complete writing process on the file
        workbook.write(fileOutputStream);

        //close the file after writing completion
        fileOutputStream.close();




    }
}
