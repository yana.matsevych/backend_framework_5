package api.tdd.pet_store;

import api.pojo_classes.pet_store.AddAPet;
import api.pojo_classes.pet_store.Category;
import api.pojo_classes.pet_store.Tags;
import api.pojo_classes.pet_store.UpdateAPet;
import com.jayway.jsonpath.JsonPath;
import groovy.json.JsonOutput;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import utils.ConfigReader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class AddPetToStoreWithLombok {
    static Logger logger = LogManager.getLogger(AddPetToStoreWithLombok.class);
    Response response;

    @BeforeSuite
    public void testStarts(){
        logger.info("Starting the test suite");
    }

    @BeforeTest
    public void beforeTest(){
        System.out.println("Starting the API test");

        //By having RestAssured URI set implisitly in the rest assure
        //we just setting path in the call
        RestAssured.baseURI = ConfigReader.getProperty("PetStoreBaseURI");
    }


    @Test
    public void addPetToStore(){

        Category category = Category
                .builder()
                .id(10)
                .name("horse")
                .build();

        Tags tags0 = Tags
                .builder()
                .id(15)
                .name("unicorn")
                .build();

        Tags tags1 = Tags
                .builder()
                .id(16)
                .name("pearl")
                .build();

        AddAPet addAPet = AddAPet
                .builder()
                .id(8)
                .category(category)
                .name("Rainbow")
                .photoUrls(Arrays.asList("My horse's photo URL"))
                .tags(Arrays.asList(tags0, tags1))
                .status("available")
                .build();


        response = RestAssured
                .given().log().all()
                .contentType(ContentType.JSON)
                .body(addAPet)
                .when().post("/v2/pet")
                .then().log().all()
                .assertThat().statusCode(200)
                .extract().response();

        //response body
        int actualPetId = response.jsonPath().getInt("id");  //category.id
        int actualTegId0 = response.jsonPath().getInt("tags[0].id");

        int actualPetIdWithJayway = JsonPath.read(response.asString(), "id");
        logger.info("My id with Jayway is " + actualPetIdWithJayway);

        int actualTehId0withJayWay = JsonPath.read(response.asString(), "tags[0].id");
        logger.info("My pet tag id with JayWay  id " + actualTehId0withJayWay);

        //request body
        int expectedPetId = addAPet.getId();
//        int expectedPetId = 3;

        int expectedTagsId = tags0.getId();

        //we are logging the information
        logger.info("My actual pet id is " + actualPetId);

        //we are debugging the assertion
        logger.debug("The actual pet id should be " + expectedPetId + " but we found " + actualPetId);
//        Assert.assertEquals(actualPetId, expectedPetId);

        //Assertion with Hamcrest

        assertThat(
                //reason why we are asserting
                "I am checking if the " + expectedPetId + " is matching with " + actualPetIdWithJayway,
                //actual value
                actualPetIdWithJayway,
                //expected value
                is(expectedPetId)
        );


        int actualCategoryIdWithJayWay = JsonPath.read(response.asString(), "category.id");

        logger.debug("The actual pet id should be " + category.getId() + " but we found " + actualCategoryIdWithJayWay);
        assertThat(
                "I am validating the category id",
                // actual value
                actualCategoryIdWithJayWay,
                // expected value
                is(category.getId())
        );


        System.out.println("Update the path");

        Category updateCategory = Category
                .builder()
                .id(11)
                .name("horse")
                .build();

        UpdateAPet updateAPet = UpdateAPet
                .builder()
                .id(8)
                .category(updateCategory)
                .name("Rainbow")
                .photoUrls(Arrays.asList("My horse's photo URL"))
                .tags(Arrays.asList(tags0, tags1))
                .status("unavailable")
                .build();

        response = RestAssured
                .given().log().all()
                .contentType(ContentType.JSON)
                .body(updateAPet)
                .when().put("/v2/pet")
                .then().log().all()
                .assertThat().statusCode(200)
                .extract().response();





    }

}
