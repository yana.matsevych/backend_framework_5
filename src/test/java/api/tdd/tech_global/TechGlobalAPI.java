package api.tdd.tech_global;

import api.pojo_classes.tech_global.PostStudent;
import com.github.javafaker.Faker;
import com.jayway.jsonpath.JsonPath;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.Test;
import utils.ConfigReader;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.is;

public class TechGlobalAPI {
    static Logger logger = LogManager.getLogger(TechGlobalAPI.class);
    Faker faker = new Faker();
    Response response;

    @Test
    public void postStudent(){

        logger.info("Beginning of the test");

        String expectedFirstName = faker.name().firstName();
        String expectedLastName = faker.name().lastName();
        String expectedEmail = faker.internet().emailAddress();
        String expectedDOB = "1980-09-23";

        PostStudent postStudent = PostStudent
                .builder()
                .firstName(expectedFirstName)
                .lastName(expectedLastName)
                .email(expectedEmail)
                .dob(expectedDOB)
                .build();


        response = RestAssured
                .given().log().all()
                .contentType(ContentType.JSON)
                .body(postStudent)
                .when().post(ConfigReader.getProperty("TechGlobalURI"))
                .then().log().all()
                .assertThat().statusCode(200)
                .extract().response();

        String actualFirstName = JsonPath.read(response.asString(), "firstName");
        String actualLastName = JsonPath.read(response.asString(), "lastName");
        String actualEmail = JsonPath.read(response.asString(), "email");
        String actualDOB = JsonPath.read(response.asString(), "dob");

        logger.debug("The expected first name is " + expectedFirstName + " and we found " + actualFirstName);
        assertThat("Checking if the expected first name is matching with actual first name",
                actualFirstName,
           is(expectedFirstName));

        logger.debug("The expected last name is " + expectedLastName + " and we found " + actualLastName);
        assertThat("Checking if the expected last name is matching with actual last name",
                actualLastName,
                is(expectedLastName));

        logger.debug("The expected email is " + expectedEmail + " and we found " + actualEmail);
        assertThat("Checking if the expected email is matching with actual email",
                actualEmail,
                is(expectedEmail));

        logger.debug("The expected date of birth is " + expectedDOB + " and we found " + actualDOB);
        assertThat("Checking if the expected date of birth is matching with actual date of birth",
                actualDOB,
                is(expectedDOB));

        
    }

}