package api.tdd.tech_global;

import com.github.javafaker.Faker;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;

public class APIAutomationSample {
    public static void main(String[] args) {

        String str = "java";
        str = str.replaceAll("[^a]", "");
        System.out.println(str);

        /**
         * Response is an interface coming from RestAssure Library
         * The response variable "response" stores all the components of API calls
         * including the request and response
         * RestAssured is written with BDD flow
         *
         */
        Response response;

        Faker faker = new Faker();


        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer ffb38c2bc106c7fc20811c6b429ff4bd5833d3720e1c4a9172db128a4ce3cade")
                .body("{\n" +
                        "    \"name\": \"" + faker.name().fullName() +"\",\n" +
                        "    \"gender\": \"male\",\n" +
                        "    \"email\": \"" + faker.internet().emailAddress() +"\",\n" +
                        "    \"status\": \"active\"\n" +
                        "}")
                .when().post("https://gorest.co.in/public/v2/users")
                .then().log().all().extract().response();

       // System.out.println(response.asString());

        int postId =response.jsonPath().getInt("id");

        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer ffb38c2bc106c7fc20811c6b429ff4bd5833d3720e1c4a9172db128a4ce3cade")
                .when().get("https://gorest.co.in/public/v2/users/" + postId)
                .then().log().all().extract().response();

//        RestAssured
//                .given().log().all()
//                .header("Content-Type", "application/json")
//                .header("Authorization", "Bearer ffb38c2bc106c7fc20811c6b429ff4bd5833d3720e1c4a9172db128a4ce3cade")
//                .when().get("https://gorest.co.in/public/v2/users")
//                .then().log().all().extract().response();

        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer ffb38c2bc106c7fc20811c6b429ff4bd5833d3720e1c4a9172db128a4ce3cade")
                .body("{\n" +
                        "    \"name\": \"" + faker.name().fullName() +"\",\n" +
                        "    \"gender\": \"male\",\n" +
                        "    \"status\": \"inactive\"\n" +
                        "}")
                .when().put("https://gorest.co.in/public/v2/users/" + postId)
                .then().log().all().extract().response();

        int patchId = response.jsonPath().getInt("id");

        Assert.assertEquals(postId, patchId, "Expected id" + postId + "patch is" + patchId);

        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer ffb38c2bc106c7fc20811c6b429ff4bd5833d3720e1c4a9172db128a4ce3cade")
                .when().delete("https://gorest.co.in/public/v2/users/" + postId)
                .then().log().all().extract().response();


    }
}
