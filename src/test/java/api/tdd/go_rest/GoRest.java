package api.tdd.go_rest;

import api.pojo_classes.go_rest.CreateGoRestUsers;
import api.pojo_classes.go_rest.UpdateGoRestUser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import com.jayway.jsonpath.JsonPath;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hamcrest.Matchers;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import utils.ConfigReader;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class GoRest {
    static Logger logger = LogManager.getLogger(GoRest.class);
    Response response;
    ObjectMapper objectMapper = new ObjectMapper();
    CreateGoRestUsers createGoRestUsers = new CreateGoRestUsers();
    Faker faker = new Faker();
    int expectedId;
    String expectedName;
    String expectedEmail;
    String expectedGender;



    @BeforeTest
    public void beforeTest(){
        System.out.println("Starting the API test");

        //By having RestAssured URI set implisitly in the rest assure
        //we just setting path in the call
        RestAssured.baseURI = ConfigReader.getProperty("GoRestBaseURI");
    }

    @Test
    public void goRestCRUD() throws JsonProcessingException {


        /**
         * ObjectManager to convert Java object to Json
         * Creating a Bean object
         */
        createGoRestUsers.setName("Yana Matsevych");
        createGoRestUsers.setGender("female");
        createGoRestUsers.setEmail(faker.internet().emailAddress());
        createGoRestUsers.setStatus("active");

        response = RestAssured
                .given().log().all()
//                .header("Content-Type", "application/json")
                .contentType(ContentType.JSON)
                .header("Authorization", ConfigReader.getProperty("GoRestToken"))
                .body(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(createGoRestUsers))
                .when().post("/public/v2/users")
                .then().log().all()
                //validating status code
                .and().assertThat().statusCode(201)
                //validating the response time is lees then specified
                .time(Matchers.lessThan(9000L))
                //validating the value  from the body with hamcrest
                .body("name", equalTo("Yana Matsevych"))
                //validating response content body
                .contentType(ContentType.JSON)
                .extract().response();

        String expectedStatus = createGoRestUsers.getStatus();
        String actualStatus = JsonPath.read(response.asString(), "status");
        logger.debug("Expected code is " + expectedStatus + ". I got " + actualStatus);

        assertThat(
                "I am checking if the " + expectedStatus + " is matching with " + actualStatus,

                actualStatus,

                is(expectedStatus)
        );



        expectedId = response.jsonPath().getInt("id");

        response = RestAssured
                .given().log().all()
                .contentType(ContentType.JSON)
                .header("Authorization", ConfigReader.getProperty("GoRestToken"))
                .when().get("/public/v2/users/" + expectedId)
                .then().log().all()
                .and().assertThat().statusCode(200)
                //validating the response time is lees then specified
                .time(Matchers.lessThan(9000L))
                //validating the value  from the body with hamcrest
                .body("name", equalTo("Yana Matsevych"))
                //validating response content body
                .contentType(ContentType.JSON)
                .extract().response();

//        createGoRestUsers.setName("John Doe");
//        String name = createGoRestUsers.getName();

        UpdateGoRestUser updateGoRestUser = new UpdateGoRestUser();
        updateGoRestUser.setName("Jane Smith");
        String nameUpdated = updateGoRestUser.getName();
        updateGoRestUser.setEmail(faker.internet().emailAddress());

                response = RestAssured
                .given().log().all()
//                .header("Content-Type", "application/json")
                .contentType(ContentType.JSON)
                .header("Authorization", ConfigReader.getProperty("GoRestToken"))
                .body(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(updateGoRestUser))
                .when().put("/public/v2/users/" + expectedId)
                .then().log().all()
                //validating status code
                .and().assertThat().statusCode(200)
                //validating the response time is lees then specified
                .time(Matchers.lessThan(9000L))
                //validating the value  from the body with hamcrest
                .body("name", equalTo(nameUpdated))
                //validating response content body
                .contentType(ContentType.JSON)
                .extract().response();


                expectedName = updateGoRestUser.getName();
                expectedEmail = updateGoRestUser.getEmail();
                expectedGender = createGoRestUsers.getGender();
                expectedStatus = createGoRestUsers.getStatus();

                int actualId = response.jsonPath().getInt("id");
                String actualName = response.jsonPath().getString("name");
                String actualEmail = response.jsonPath().getString("email");
                String actualGender = response.jsonPath().getString("gender");
                String actualStatuses = response.jsonPath().getString("status");

        Assert.assertEquals(actualId, expectedId);
        Assert.assertEquals(actualName, expectedName);
        Assert.assertEquals(actualEmail, expectedEmail);
        Assert.assertEquals(actualGender, expectedGender);
        Assert.assertEquals(actualStatuses, expectedStatus);

        System.out.println("===========DELETE user=========");

        response = RestAssured
                .given().log().all()
//                .header("Content-Type", "application/json")
                .contentType(ContentType.JSON)
                .header("Authorization", ConfigReader.getProperty("GoRestToken"))
                .when().delete("/public/v2/users/" + expectedId)
                .then().log().all()
                //validating status code
                .and().assertThat().statusCode(204)
                //validating the response time is lees then specified
                .time(Matchers.lessThan(9000L))
                //validating the value  from the body with hamcrest
                .extract().response();

    }
}