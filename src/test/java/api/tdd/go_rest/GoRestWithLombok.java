package api.tdd.go_rest;

import api.pojo_classes.go_rest.CreateGoRestUserWithLombok;
import api.pojo_classes.go_rest.CreateGoRestUsers;
import api.pojo_classes.go_rest.UpdateGoRestUser;
import api.pojo_classes.go_rest.UpdateGoRestUserWithLombok;
import api.tdd.pet_store.AddPetToStoreWithLombok;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import com.jayway.jsonpath.JsonPath;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hamcrest.Matchers;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import utils.ConfigReader;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class GoRestWithLombok {

    static Logger logger = LogManager.getLogger(GoRestWithLombok.class);
    Response response;
    ObjectMapper objectMapper = new ObjectMapper();
    CreateGoRestUsers createGoRestUsers = new CreateGoRestUsers();
    Faker faker = new Faker();
    int expectedId;
    String expectedName;
    String expectedEmail;
    String expectedGender;
    String expectedStatus;


    @BeforeTest
    public void beforeTest(){
        System.out.println("Starting the API test");

        //By having RestAssured URI set implisitly in the rest assure
        //we just setting path in the call
        RestAssured.baseURI = ConfigReader.getProperty("GoRestBaseURI");
    }

    @Test
    public void goRestCRUDWithLombok() throws JsonProcessingException {

        CreateGoRestUserWithLombok createUser = CreateGoRestUserWithLombok
                .builder()
                .name("Jan Sope")
                .email(faker.internet().emailAddress())
                .gender("female")
                .status("active")
                .build();


        response = RestAssured
                .given().log().all()
//                .header("Content-Type", "application/json")
                .contentType(ContentType.JSON)
                .header("Authorization", ConfigReader.getProperty("GoRestToken"))
                .body(createUser)
                .when().post("/public/v2/users")
                .then().log().all()
                //validating status code
                .and().assertThat().statusCode(201)
                //validating the response time is lees then specified
                .time(Matchers.lessThan(9000L))
                //validating the value  from the body with hamcrest
                .body("name", equalTo("Jan Sope"))
                //validating response content body
                .contentType(ContentType.JSON)
                .extract().response();

        int userId = response.jsonPath().getInt("id");

        String actualName = JsonPath.read(response.asString(), "name");
        String expectedName = createUser.getName();
        logger.debug("This is actual name " + actualName);
        logger.debug("This is expected name " + expectedName);


        assertThat(
                "I am checking if the " + expectedName + " is matching with " + actualName,

                actualName,

                is(expectedName)
        );

        response = RestAssured
                .given().log().all()
//                .header("Content-Type", "application/json")
                .contentType(ContentType.JSON)
                .header("Authorization", ConfigReader.getProperty("GoRestToken"))
                .when().get("/public/v2/users/" + userId)
                .then().log().all()
                //validating status code
                .and().assertThat().statusCode(200)
                //validating the response time is lees then specified
                .time(Matchers.lessThan(8000L))
                //validating the value  from the body with hamcrest
                .extract().response();


        UpdateGoRestUserWithLombok updateUser = UpdateGoRestUserWithLombok
                //building the update java body
                .builder()
                .email(faker.internet().emailAddress())
                .gender("male")
                .status("inactive")
                .build();

        response = RestAssured
                .given().log().all()
//                .header("Content-Type", "application/json")
                .contentType(ContentType.JSON)
                .header("Authorization", ConfigReader.getProperty("GoRestToken"))
                .body(updateUser)
                .when().put("/public/v2/users/" + userId)
                .then().log().all()
                //validating status code
                .and().assertThat().statusCode(200)
                //validating the response time is lees then specified
                .time(Matchers.lessThan(8000L))
                //validating the value  from the body with hamcrest
                .extract().response();

        response = RestAssured
                .given().log().all()
//                .header("Content-Type", "application/json")
                .contentType(ContentType.JSON)
                .header("Authorization", ConfigReader.getProperty("GoRestToken"))
                .when().delete("/public/v2/users/" + userId)
                .then().log().all()
                //validating status code
                .and().assertThat().statusCode(204)
                //validating the response time is lees then specified
                .time(Matchers.lessThan(8000L))
                //validating the value  from the body with hamcrest
                .extract().response();

    }
}