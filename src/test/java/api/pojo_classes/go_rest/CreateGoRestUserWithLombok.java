package api.pojo_classes.go_rest;

import lombok.Builder;
import lombok.Data;

/**
 * With the help of dat which is coming from lombok we can eliminate getters and setters
 */
@Data

/**
 * With @Builder, we are able to assign the value to the attributes
 */
@Builder

public class CreateGoRestUserWithLombok {

    /**
     * {
     * "name": "",
     * "gender": "male",
     * "email": "ym@gmail.com",
     * "status": ""
     * }
     */

    private String name;
    private String gender;
    private String email;
    private String status;



}


