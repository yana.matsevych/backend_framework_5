package database;

import java.sql.*;

public class DatabaseConnection {
    public static void main(String[] args) throws SQLException {


        /**
         * in order to connect to the database, we need our URL, username , password, query
         * THIS IS INTERVIEW QUESTION
         */

        String url = "jdbc:oracle:thin:@techglobal.cup7q3kvh5as.us-east-2.rds.amazonaws.com:1521/TGDEVQA";
        String username = "yoanna";
        String password = "$yoanna123!";
        String query = "select * from employees";

        //create a connection to the database with the parameters we stored
        Connection connection = DriverManager.getConnection(url, username, password);

        System.out.println("Database connection is successful");

        /**
         * Statement keeps the connections between B and Automation
         * to send query
         */

        Statement statement = connection.createStatement();


        // resultSet is sending the query to the database and set the result
        ResultSet resultSet = statement.executeQuery(query);


        /**
         * ResultSetMetaData gives the information about the table
         * You cant simply print the column values. We need to call them with iterations
         */
        ResultSetMetaData resultSetMetaData = resultSet.getMetaData();

        System.out.println("Number of column " + resultSetMetaData.getColumnCount());
        System.out.println("Column name " + resultSetMetaData.getColumnName(1));

        /**
         * Process the result set
         */
        while(resultSet.next()){
            System.out.print(resultSet.getString("FIRST_NAME") + " ");
            System.out.println(resultSet.getString("LAST_NAME"));
        }

    }
}
