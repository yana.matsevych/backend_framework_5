package utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class ExcelUtil {

    static Logger logger = LogManager.getLogger(ExcelUtil.class);
    private static XSSFWorkbook workbook;
    private static XSSFSheet sheet;
    private static XSSFRow row;
    private static XSSFCell cell;
    private static String filepath;


    public static void openExcelFile(String fileName, String sheetName){
        filepath = "test_data/" + fileName + ".xlsx";

        try{
            FileInputStream fileInputStream = new FileInputStream(filepath);
            workbook = new XSSFWorkbook(fileInputStream);
            logger.info("File " + fileName + " exist");
            sheet = workbook.getSheet(sheetName);
            logger.info("Sheet " + sheetName + " exist");
        }catch (Exception e){
            logger.debug(fileName + " and " + sheetName + " can not be found");
        }

    }


    public static String getValue(int rowNumber, int cellNumber){
        row = sheet.getRow(rowNumber);
        cell = row.getCell(cellNumber);
        cell.setCellType(CellType.STRING);
        return cell.toString();
    }

    public static List<List<String>> getValues(){
        //creating List of List to tore all the values from the Excel file
        List<List<String>> allValues = new ArrayList<>();
        //creating loops for getting the rows
        for(int r = sheet.getFirstRowNum() + 1; r <= sheet.getLastRowNum(); r++){
            // creating a row in the sheet
            row = sheet.getRow(r);
            //creating list for storing row data
            List<String> eachRow = new ArrayList<>();
            //lopping each cell in the corresponding row
            for(int c = row.getFirstCellNum(); c < row.getLastCellNum(); c++){
                //get each cell values and add them into each row list
                eachRow.add(getValue(r, c));
            }
            //adding each list of the row in the list of the list
            allValues.add(eachRow);

        }
        //returning list of the list
        return allValues;
    }


    public static String[][] getExelData(List<List<String>> listOfList){
        //creating multidimensional array

        String[][] result = new String[listOfList.size()][listOfList.get(0).size()];

        //loop the lists
        for (int i = 0; i < listOfList.size(); i++) {
            //getting the values from each list
            for (int j = 0; j < listOfList.get(i).size(); j++) {
                result[i][j] = listOfList.get(i).get(j);
            }
        }

        return result;

    }

    //closing workbook

    public static void closingExcelFile(){
        try{
            workbook.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }


}
