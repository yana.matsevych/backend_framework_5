package utils;

import org.testng.annotations.DataProvider;

public class DataProvideUtil {

    @DataProvider(name = "DataFromExcel")
    public static Object[][] getDataFromExcelFileWithDataProvider(){

        //Opening ExcelFile
        ExcelUtil.openExcelFile("PetDataToRead","Sheet1");
        //Storing converted data to multidimensional array
        Object[][] dataArray = ExcelUtil.getExelData(ExcelUtil.getValues());
        //closing the Excel file
        ExcelUtil.closingExcelFile();

        return dataArray;
    }


}
