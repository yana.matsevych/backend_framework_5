package stepDef.apiSteDef.go_rest;

import api.pojo_classes.go_rest.CreateGoRestUserWithLombok;
import api.tdd.go_rest.GoRestWithLombok;
import com.github.javafaker.Faker;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hamcrest.Matchers;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import utils.ConfigReader;

import static org.hamcrest.Matchers.equalTo;

public class GoRestStepDef {

    static Logger logger = LogManager.getLogger(GoRestWithLombok.class);
    Response response;
    Faker faker = new Faker();

    @BeforeTest
    public static void beforeTest() {
    RestAssured.baseURI =ConfigReader.getProperty("GoRestBaseURI");
}
    @Given("I send a POST request with body")
    public void iSendAPOSTRequestWithBody() {
        CreateGoRestUserWithLombok createUser = CreateGoRestUserWithLombok
                .builder()
                .name("Jan Sope")
                .email(faker.internet().emailAddress())
                .gender("female")
                .status("active")
                .build();


        response = RestAssured
                .given().log().all()
//                .header("Content-Type", "application/json")
                .contentType(ContentType.JSON)
                .header("Authorization", ConfigReader.getProperty("GoRestToken"))
                .body(createUser)
                .when().post("/public/v2/users")
                .then().log().all()
                //validating status code
                .and().assertThat().statusCode(201)
                //validating the response time is lees then specified
                .time(Matchers.lessThan(9000L))
                //validating the value  from the body with hamcrest
                .body("name", equalTo("Jan Sope"))
                //validating response content body
                .contentType(ContentType.JSON)
                .extract().response();
    }

    @Then("Status code is {int}")
    public void statusCodeIs(int arg0) {
        Assert.assertEquals(response.getStatusCode(), 201);
    }
}
